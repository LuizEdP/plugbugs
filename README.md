# Relação de alguns bugs conhecidos no plug.dj

Estão listados e detalhados alguns bugs conhecidos. A lista será atualizada conforme novos bugs apareçam ou enquanto os existentes forem corrigidos ou que saibamos algo novo sobre ele.

- [Fantasmas (ghosts)](https://gitlab.com/LuizEdP/plugbugs/blob/master/ghosts.md)
- [Usuário na fila, mas não na sala](https://gitlab.com/LuizEdP/plugbugs/blob/master/waitlistghost.md)
- Item da playlist repetindo
- [Alguns avatares não funcionam](https://gitlab.com/LuizEdP/plugbugs/blob/master/avatarscors.md)
- [Ganhos de PPs e XPs com atrasos de alguns dias](https://gitlab.com/LuizEdP/plugbugs/blob/master/earndelay.md)
- [Após entrar na sala, o chat deixa de funcionar e o DJ não avança](https://gitlab.com/LuizEdP/plugbugs/blob/master/socketdied.md)