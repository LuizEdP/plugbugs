# Bug de fantasmas (ghosts)

O bug de fantasma teve grande notoriedade meses após a "grande atualização" que ocorreu em agosto de 2014. Conforme algumas falhas foram corrigidas, os casos de fantasmas não intencionais se tornaram escassos, embora essa falha esteja presente até os dias atuais.

## Princípio básico
O site utiliza de dois protocolos de comunicação para sua operação: *HTTP* e *WebSocket*, sendo o segundo tecnicamente provido por meio do primeiro. A menos que você seja programar, deduzo que você está se perguntando que coisas são essas. Vamos então a alguns exemplos práticos de onde são usados:

* HTTP
    * Entrar na lista de espera
    * Sair da lista de espera
    * Pular DJ
    * Trancar/destrancar lista de espera
    * Criar playlist
    * Ativar playlist
    * Adicionar item na playlist
    * Excluir mensagens
* WebSocket
    * Chat
    * Notificação de término de uma música e início de outra
    * Notificação de entrada e saída de usuários
    * Demais notificações em tempo real enviada pelo site

Como podemos notar, os serviços de HTTP representam requisições originadas pelo usuário e que são processadas pelo servidor e, em alguns casos, transmitidas aos demais usuários por meio do WebSocket, em que os usuários receberão essas informações por meio de notificações e também pela rotina padrão do site. De modo mais sucinto, o serviço de WebSocket provê comunicação em tempo real e bidirecional entre usuários e servidor e com duração indefinida, ao contrário da comunicação HTTP que somente é originada pelo usuário e processada e encerrada pelo servidor (ou seja, assim que a requisição é processada, a comunicação se encerra).
Dada a explanação entre HTTP e WebSocket, você agora compreende que o site funciona por meio de duas formas de comunicação entre navegador e servidor. 

Mas e então, em que isto tem relação com o bug de ghost? Como veremos a seguir, o bug somente ocorre sem a presença do WebSocket ou com falha do serviço de WebSocket em execução no servidor do site.

## Como a falha acontece
O plug possui uma maneira peculiar de tratar a informação de desconexão de usuário, ocasionada quando você fecha o navegador, sua internet cai, enfim, quando você sai do site. A informação de desconexão é detectada pelo WebSocket, uma vez que ele reconhece o momento exato em que você se conecta e também que você se desconecta dele. Quando você se desconecta e ele reconhece, o mesmo aguarda alguns segundos até que você se conecte novamente. Caso isso não ocorre, ele então notifica a outra parte do servidor que você não está mais acessando o site no momento.
O projeto existe falhas. Caso o serviço de WebSocket enfrente algum problema, ele reinicia ou encerra e todo esse monitoramento é perdido. Com isso, ele não cumprirá com uma de suas funções: transmitir ao outro serviço que a conexão com o usuário foi perdida, ocasionando em usuários online no site mesmo quando ele não estiver de fato.
Quando o servidor apresenta falhas, os administradores necessitam reiniciar o serviço para desconectar todas as contas "presas" ao site e redirecionam o site ao modo de manutenção, bem conhecido e muitas vezes frustrante.

## Efeitos colaterais
As seguintes situações ocorrem quando um usuário possui comportamento similar a ghost, mas não necessariamente eles sejam:

* Ao entrar na sala não é gerada a notificação, mas ao sair o contador de usuários é decrescido em 1, gerando inconsistência no contador de usuários, tanto no contador de usuários registrados como no de guest. Em casos extremos, o contador pode ser decrescido a zero mesmo havendo outras pessoas na sala (ao recarregar a página, o contador se corrige);
* Caso não haja DJ tocando e um usuário que não apareça na lista de usuários entre na fila, o usuário é exibido na cabine mas as informações da música que está tocando no momento continua exibindo que ninguém está tocando.

Em nível técnico:

* Os eventos de userLeave e advance são emitidos normalmente, sendo o primeiro contendo apenas o ID do usuário (em caso de guest, é enviado um zero) e o segundo os dados de advance contendo os usuários na fila, o DJ, a mídia, ID da playlist do DJ, data do evento e ID único;
* A página exibe os dados fazendo uso dos dados de usuários armazenados por ela. Os novos usuários são adicionados a essa lista por meio do evento de userJoin, mas como o evento não é disparado a página não possui esses dados e, quando requisitado, provoca inconsistências como as listadas acima;
* Em grande parte dos eventos emitidos pelo servidor, o usuário é obtido no servidor caso ele não esteja na lista do frontend. Como essa requisição pode levar alguns milissegundos ou segundos para ser concluída, alguns eventos são emitidos de forma tardia para outras partes do código da página quando a requisição é concluída;
* Caso o usuário não apareça na lista de usuários mas esteja na sala, o mesmo ficará visível ao recarregar a página ou se ele tomar alguma ação que force a página, nos demais usuários, a carregar os dados dele.