# Após entrar na sala, o chat deixa de funcionar e o DJ não avança

Este bug atinge cada vez mais usuários com o passar do tempo e ainda persiste. Confira abaixo alguns detalhes sobre e uma maneira de contornar o transtorno.

## Como ocorre
O bug se trata de um problema no serviço do WebSocket (caso não tenha conhecimento a respeito de WebSocket no contexto do plug, [leia aqui](https://gitlab.com/LuizEdP/plugbugs/blob/master/ghosts.md#princ%C3%ADpio-b%C3%A1sico)) em que o usuário é capaz de enviar mensagens para o servidor e outros usuários, mas incapaz de receber as mensagens do servidor. Desse modo, ele não é capaz de receber novas mensagens nem de reconhecer quando o DJ avança, alguma ação de staff ocorre, dentre outros eventos em tempo real.

## Como contornar o problema
Esta é uma medida paleativa a ser adotada enquanto o bug não é corrigido. Este procedimento foi repassado pela equipe do plug [neste documento do Google Docs](https://docs.google.com/document/d/11QTmn9rBtJKUqKBz3l_5ZRr2CwxdhlsJMKpcrPtTQO4/edit) e abaixo descrevemos de modo mais detalhado a maneira de ser feito:

1) Entre em alguma sala que você deseja;

2) Assim que o seu avatar aparecer na tela de _entrando_ ou o chat ficar visível, clique com o mouse direito sobre a aba do plug, clique em *duplicar* e NÃO FECHE A ABA BUGADA JÁ ABERTA;

3) Vá para a aba que foi duplicada e aguarde ser carregada;

4) Verifique se nos primeiros segundos você é capaz de receber as mensagens e eventos da sala, para isso pode-se enviar mensagens no chat.