# Usuário na fila, mas não na sala
Sim, você não leu errado. Neste bug o usuário sai do site ou da sala, mas não sai da lista de espera da sala em que estiver.

## Mas como assim?
Não conseguimos forçar esse bug. Aparentemente não existe uma situação propícia a ocorrer, mas não podemos afirmar. O máximo que podemos fazer é entender esse bug após acontecer.
Ao sair da sala, você também sairá da fila. Mas isso pode não acontecer: você sai da sala, mas ainda consta como estando na lista de espera e é aí que encontramos o bug.

Características dessa situação:
* Você permanecerá na fila enquanto não for removido pela staff, voltar à sala para sair da fila ou enquanto o site não entrar em manutenção, embora já presenciamos usuários presos na lista de espera após uma manutenção;
* Você não fica online e não ganha pontuações por isso.

Sem mais detalhes, somente uma conclusão é possível, citando o ilustre Chicó: _"Não sei, só sei que foi assim"_.